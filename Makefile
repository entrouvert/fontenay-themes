VERSION=`git describe | sed 's/^v//; s/-/./g' `
NAME="fontenay-themes"

prefix = /usr

all:
	(cd wcs/fontenay && msgfmt -o fr.mo fr.po)

clean:
	rm -f wcs/fontenay/fr.mo
	rm -rf build sdist

build: clean
	mkdir -p build/$(NAME)-$(VERSION)
	for i in *; do \
		if [ "$$i" != "build" ]; then \
			cp -R "$$i" build/$(NAME)-$(VERSION); \
		fi; \
	done

install:
	mkdir -p $(DESTDIR)$(prefix)/share/fontenay-theme/templates
	mkdir -p $(DESTDIR)$(prefix)/share/wcs/themes/fontenay
	mkdir -p $(DESTDIR)$(prefix)/share/fontenay-theme/static
	cp -r static/* $(DESTDIR)$(prefix)/share/fontenay-theme/static
	cp -r templates/* $(DESTDIR)$(prefix)/share/fontenay-theme/templates/
	cp -ar wcs/fontenay/* $(DESTDIR)$(prefix)/share/wcs/themes/fontenay/

dist-bzip2: build
	mkdir sdist
	cd build && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 .

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))

