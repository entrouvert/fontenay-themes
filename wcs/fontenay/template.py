#! /usr/bin/env python

class RemoteTemplate(object):
    def __init__(self, source):
        self.source = source
        from wcs.qommon.misc import simplify
        import os, urlparse
        self.cache_key = simplify(urlparse.urlunparse(urlparse.urlparse(self.source)[:3] + ('', '', '')))
        self.cache_dirpath = os.path.join(publisher.app_dir, 'skeleton-cache')
        self.cache_filepath = os.path.join(self.cache_dirpath, self.cache_key)

    def get_template_content(self):
        import time, os, threading
        CACHE_REFRESH_TIMEOUT = 300
        try:
            cache_filepath_mtime = os.stat(self.cache_filepath).st_mtime
        except OSError: # missing file
            return self.update_content()
        else:
            template_content = open(self.cache_filepath).read()
            if time.time() > (cache_filepath_mtime + CACHE_REFRESH_TIMEOUT):
                threading.Thread(target=self.update_content).start()
            return template_content

    def update_content(self):
        import requests
        theme_skeleton_url = publisher.get_site_option('theme_skeleton_url')

        r = requests.get(theme_skeleton_url, params={'source': self.source, 'format': 'ezt'})

        body = """
             [if-any gauche]
             <div id="gauche">
             [gauche]
             </div>
             [end]

             [if-any bigdiv]<div id="[bigdiv]" [if-any breadcrumb]class="has-breadcrumb"[end]>[end]

             [if-any title]<h2>[title]</h2>[end]
             [body]
             [if-any bigdiv]</div>[end]
        """

        extra_head = """
[script]
<script type="text/javascript" src="/themes/base/static/dataview.js"></script>
<link rel="stylesheet" type="text/css" href="[css]">
"""

        extra_top_head = """
<link rel="stylesheet" type="text/css" href="/qo/css/qommon.css">
"""

        user_info = """
          [if-any user]
          <span class="logged-in">
            <span class="connected-user">[session_user_display_name]</span>
            <a class="logout" href="[root_url]logout">D&eacute;connexion</a>
          </span>
          [else]
          <span class="login"><a href="[root_url]login/">Connexion</a> / <a href="[root_url]register/">Inscription</a></span>
          [end]

          [if-any user]
          [is session_user_admin_access "True"]
          <a class="restricted" href="[root_url]admin/">Administration</a>
          [else]
            [is session_user_backoffice_access "True"]
              <a class="restricted" href="[root_url]backoffice/">Back office</a>
            [end]
          [end]
          [end]
          """

        template_content = r.text.encode('utf-8')
        template_content = template_content.replace('[if-any content][content][end]', body)
        template_content = template_content.replace('[if-any extra-top-head][extra-top-head][end]', extra_top_head)
        template_content = template_content.replace('[if-any extra-head][extra-head][end]', extra_head)
        template_content = template_content.replace('[if-any user-info][user-info][end]', user_info)
        self.cache(template_content)
        return template_content

    def cache(self, template_body):
        import os
        from qommon.storage import atomic_write
        if not os.path.exists(self.cache_dirpath):
            os.mkdir(self.cache_dirpath)
        atomic_write(self.cache_filepath, template_body)

template_content = RemoteTemplate(request.get_url()).get_template_content()
